create schema mondial;
set search_path to mondial;

drop table if exists city;
create table city (
  city_id char(6) not null,
  province_id char(6) not null,
  name varchar(100) not null,
  population int default null,
  longitude decimal(7,4) default null,
  latitude decimal(7,4) default null,
  primary key (city_id)
);

drop table if exists continent;
create table continent (
  continent_id char(6) not null,
  name varchar(100) not null,
  area decimal(9,1) not null,
  primary key (continent_id)
);

drop table if exists country;
create table country (
  country_code varchar(4) not null,
  name varchar(100) not null,
  area decimal(9,1) not null,
  population int not null,
  population_growth decimal(5,2) default null,
  infant_mortality decimal(5,2) default null,
  gdp decimal(9,1) default null,
  inflation decimal(4,1) default null,
  agriculture decimal(4,1) default null,
  service decimal(4,1) default null,
  industry decimal(4,1) default null,
  independence date default null,
  government varchar(150) default null,
  dependent_on varchar(4) default null,
  capital_id char(6) default null,
  primary key (country_code),
  constraint fk_country_capital foreign key (capital_id) references city (city_id) deferrable,
  constraint fk_country_dependent foreign key (dependent_on) references country (country_code) deferrable
);

drop table if exists province;
create table province (
  province_id char(6) not null,
  country_code varchar(4) not null,
  name varchar(100) not null,
  population int not null,
  area decimal(9,1) default null,
  capital_id char(6) default null,
  primary key (province_id),
  constraint fk_province_capital foreign key (capital_id) references city (city_id) deferrable,
  constraint fk_province_country foreign key (country_code) references country (country_code) deferrable
);

alter table city add constraint fk_city_province foreign key (province_id) references province (province_id) deferrable;

drop table if exists organization;
create table organization (
  orga_id varchar(100) not null,
  name varchar(100) not null,
  city_id char(6) default null,
  established date default null,
  primary key (orga_id),
  constraint fk_organization_city foreign key (city_id) references city (city_id) deferrable
);

drop table if exists country_borders;
create table country_borders (
  country_code varchar(4) not null,
  other_country_code varchar(4) not null,
  length decimal(9,1) not null,
  primary key (country_code,other_country_code),
  constraint fk_country_borders_country foreign key (country_code) references country (country_code) deferrable,
  constraint fk_country_borders_other_country foreign key (other_country_code) references country (country_code) deferrable
);

drop table if exists country_in_organization;
create table country_in_organization (
  country_code varchar(4) not null,
  orga_id varchar(100) not null,
  status varchar(100) not null,
  primary key (country_code,orga_id),
  constraint fk_country_in_organization_country foreign key (country_code) references country (country_code) deferrable,
  constraint fk_country_in_organization_orga foreign key (orga_id) references organization (orga_id) deferrable
);

drop table if exists country_on_continent;
create table country_on_continent (
  continent_id char(6) not null,
  country_code varchar(4) not null,
  percentage decimal(4,1) not null,
  primary key (continent_id,country_code),
  constraint fk_country_on_continent_continent foreign key (continent_id) references continent (continent_id) deferrable,
  constraint fk_country_on_continent_country foreign key (country_code) references country (country_code) deferrable
);

drop type if exists culture_type;
create type culture_type as enum('religion','language','ethnic_group');

drop table if exists culture;
create table culture (
  country_code varchar(4) not null,
  name varchar(100) not null,
  type culture_type not null,
  percentage decimal(4,1) not null,
  primary key (country_code,name,type),
  constraint fk_culture_country foreign key (country_code) references country (country_code) deferrable
);

drop type if exists geo_object_type;
create type geo_object_type as enum('desert','island','lake','mountain','river','sea');

drop table if exists geo_object;
create table geo_object (
  geo_id char(6) not null,
  name varchar(100) not null,
  type geo_object_type not null,
  longitude decimal(7,4) default null,
  latitude decimal(7,4) default null,
  primary key (geo_id)
);

drop table if exists geo_object_at_city;
create table geo_object_at_city (
  geo_id char(6) not null,
  city_id char(6) not null,
  primary key (geo_id,city_id),
  constraint fk_geo_object_at_city_geo foreign key (geo_id) references geo_object (geo_id) deferrable,
  constraint fk_geo_object_at_city_city foreign key (city_id) references city (city_id) deferrable
);

drop type if exists geo_object_river_area;
create type geo_object_river_area as enum('source','estuary','source and estuary');

drop table if exists geo_object_in_province;
create table geo_object_in_province (
  geo_id char(6) not null,
  province_id char(6) not null,
  river_area geo_object_river_area default null,
  primary key (geo_id,province_id),
  constraint fk_geo_object_in_province_geo foreign key (geo_id) references geo_object (geo_id) deferrable,
  constraint fk_geo_object_in_province_province foreign key (province_id) references province (province_id) deferrable
);

drop table if exists desert;
create table desert (
  geo_id char(6) not null,
  area decimal(9,1) not null,
  primary key (geo_id),
  constraint fk_desert_geo_object foreign key (geo_id) references geo_object (geo_id) deferrable
);

drop table if exists river;
create table river (
  geo_id char(6) not null,
  length int default null,
  source_longitude decimal(7,4) default null,
  source_latitude decimal(7,4) default null,
  source_altitude decimal(5,1) default null,
  estuary_longitude decimal(7,4) default null,
  estuary_latitude decimal(7,4) default null,
  estuary_geo_id char(6) not null,
  mountains varchar(100) default null,
  primary key (geo_id),
  constraint fk_river_geo_object foreign key (geo_id) references geo_object (geo_id) deferrable,
  constraint fk_river_estuary foreign key (estuary_geo_id) references geo_object (geo_id) deferrable
);

drop type if exists lake_kind;
create type lake_kind as enum('salt','caldera','artificial','acid','impact','crater');

drop table if exists lake;
create table lake (
  geo_id char(6) not null,
  area decimal(9,1) default null,
  depth decimal(5,1) default null,
  altitude decimal(5,1) default null,
  kind lake_kind default null,
  passing_river_id char(6) default null,
  primary key (geo_id),
  constraint fk_lake_geo_object foreign key (geo_id) references geo_object (geo_id) deferrable,
  constraint fk_lake_river foreign key (passing_river_id) references river (geo_id) deferrable
);

drop table if exists island;
create table island (
  geo_id char(6) not null,
  island_group varchar(100) default null,
  area decimal(9,1) default null,
  height int default null,
  kind varchar(100) default null,
  in_lake_id char(6) default null,
  primary key (geo_id),
  constraint fk_island_geo_object foreign key (geo_id) references geo_object (geo_id) deferrable,
  constraint fk_island_lake foreign key (in_lake_id) references lake (geo_id) deferrable
);

drop table if exists mountain;
create table mountain (
  geo_id char(6) not null,
  mountain_range varchar(100) default null,
  height int not null,
  kind varchar(100) default null,
  on_island_id char(6) default null,
  primary key (geo_id),
  constraint fk_mountain_geo_object foreign key (geo_id) references geo_object (geo_id) deferrable,
  constraint fk_mountain_island foreign key (on_island_id) references island (geo_id) deferrable
);

drop table if exists sea;
create table sea (
  geo_id char(6) not null,
  depth int not null,
  primary key (geo_id),
  constraint fk_sea_geo_object foreign key (geo_id) references geo_object (geo_id) deferrable
);

drop table if exists sea_mergers;
create table sea_mergers (
  sea_id char(6) not null,
  other_sea_id char(6) not null,
  primary key (sea_id,other_sea_id),
  constraint fk_sea_mergers_sea foreign key (sea_id) references sea (geo_id),
  constraint fk_sea_mergers_other_sea foreign key (other_sea_id) references sea (geo_id) deferrable
);

drop table if exists island_in_sea;
create table island_in_sea (
  island_id char(6) not null,
  sea_id char(6) not null,
  primary key (island_id,sea_id),
  constraint fk_island_in_sea_island foreign key (island_id) references island (geo_id) deferrable,
  constraint fk_island_in_sea_sea foreign key (sea_id) references sea (geo_id) deferrable
);