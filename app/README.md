## Installation

1. Install the required database systems (DBS): [MariaDB](https://mariadb.com/kb/en/getting-installing-and-upgrading-mariadb/), [MongoDB](https://www.mongodb.com/docs/manual/administration/install-community/), and [Neo4j](https://neo4j.com/docs/operations-manual/current/installation/).
2. The application is developed in JavaScript and runs in the Node.js runtime environment on server side. Therefore, [Node.js](https://nodejs.org/en/download) has to be installed.
3. Download the latest version of the code from this repository to your server into any directory `<app-dir>`.
4. Edit configurations in `app/config.example.js` and rename it to `app/config.js`.
    - The tasks and the submissions of the participants will be saved in corresponding collections in a MongoDB database, which is named `dbql` by default.
    - The application requires read access to the Mondial databases in each DBS. Per default, a user named `mondial` with password `swordfish` is proposed for each DBS. Scripts for user creation and granting the read access can be found in `schemas/<dbs>/<dbs>_mondial_auth.*`.
    - Create the Mondial databases by executing `schemas/<dbs>/<dbs>_mondial_schema.*` and `schemas/<dbs>/<dbs>_mondial_data.*` in the corresponding DBS shells.
    - Provide paths to your TLS certificate for the application.
    - Configure the desired port and CORS settings.
    - Setup user authentication via your LDAP service.
5. Set the variables `baseUrl` and `studentDomain` in `app/client/common.js`.
6. Edit the constant strings in `app/client/resources/i18n.js` to match your organization.
7. Run `npm install` from  `<app-dir>/app/` to fetch all dependencies.
8. Run the application with `node app.js` from `<app-dir>/app/` or by means of [pm2](https://www.npmjs.com/package/pm2), e.g. `pm2 start <app-dir>/app.js --name dbql --watch` and `pm2 save`.
9. Visit the deployed app in your browser and login as one of the configured admin users. Call `https://<your-domain>/create_tasks?token=<admin-jwt>` in order to fill the tasks collection based on `app/assets/tasks.json`. You can look up a valid admin JWT in the session storage of your browser after login.