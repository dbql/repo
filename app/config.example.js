export const config = {
    db : {
        connection: 'mongodb://dbql_user:password@localhost:27017/?authSource=admin',
        name: 'dbql'
    },
    mondial: {
        mariadb: {
            connection: 'mariadb://mondial:swordfish@localhost/mondial?charset=utf8mb4&checkDuplicate=false'
        },
        mongodb: {
            connection: 'mongodb://mondial:swordfish@localhost:27017/?authSource=mondial',
            db: 'mondial'
        },
        neo4j: {
            connection: 'neo4j://localhost:7687',
            user: 'mondial',
            password: 'swordfish',
            db: 'mondial'
        },
    },
    ssl: {
        key: '/etc/letsencrypt/live/example.com/privkey.pem',
        cert: '/etc/letsencrypt/live/example.com/fullchain.pem',
        verify: true
    },
    port: 3001,
    cors: false,
	activeDirectoryEnabled: true,
    activeDirectory: {
        url: 'ldap://ldap.example.com',
        baseDN: 'dc=example,dc=com',
        username: 'ldap_user',
        password: 'password'
    },
	demoUsersEnabled: false,
	demoUsers: [
        {name: 'demo', password: 'demo'},
        {name: 'admin', password: 'admin'}
	],
    admins: ['admin'],
	jwtSecret: 'some-jwt-secret-4kzhlavx3bMK4WFfHFlc7dFrBwC9DT9y'
}