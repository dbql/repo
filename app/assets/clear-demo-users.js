// run this program as a cron job, e.g. by a daily crontab entry that runs at 04:30
// 30 4   * * *   root    node /opt/dbql/app/assets/clear-demo-users.js

import {config} from "../config.js"
import {MongoClient} from 'mongodb'

const mongo_client = await new MongoClient(config.db.connection).connect()
const conn_mongo = mongo_client.db(config.db.name)
const col_tasks = conn_mongo.collection('tasks')
const col_submissions = conn_mongo.collection('submissions')

const task_ids = ['task-1', 'task-3', 'task-4']
const solved_tasks = []

for (const task_id of task_ids) {
    const task = await col_tasks.findOne({_id: task_id})
    if (task) solved_tasks.push(task)
}

const qls = ['sql', 'mongo', 'cypher']

for (const user of config.demoUsers) {
    await col_submissions.deleteMany({user_id: user.name})

    for (const task of solved_tasks) {
        for (const ql of qls) {
            // console.log(user.name + " " + ql + " " + task._id)
            const submission = {
                user_id: user.name,
                task_id: task._id,
                ql: ql,
                date_created: Date.now(),
                text: task[ql + '_query'],
                output: task[ql + '_ref'],
                correct: true
            }
            if (ql === 'mongo') {
                submission.text = task.mongo_query.pipeline
                submission.collection = task.mongo_query.collection
            }
            await col_submissions.insertOne(submission)
        }
    }
}

mongo_client.close()