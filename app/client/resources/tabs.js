const active_class = 'is-active'

function initTabs() {
  const tabs = [...document.querySelectorAll('.tabs li')]
  tabs.forEach((tab) => {
    tab.addEventListener('click', (e) => {
      let selected = tab.getAttribute('data-tab')
      updateActiveTab(tab)
      updateActiveContent(tab, selected)
    })
  })
}

function updateActiveTab(tab) {
  const tabs = tab.parentElement.children
  for (let t of tabs) {
    t.classList.remove(active_class)
  }
  tab.classList.add(active_class)
}

function updateActiveContent(tab, selected) {
  const tabContent = tab.parentElement.parentElement.parentElement.querySelectorAll('.tab-content > div')
  for (let c of tabContent) {
      c.classList.remove(active_class)
      if (c.getAttribute('data-content') == selected) {
        c.classList.add(active_class)
      }
  }
}