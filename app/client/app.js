document.addEventListener('DOMContentLoaded', () => {
    show('demo-user')
    queryDbsVersions()

    if (token) loadTasks()

    document.getElementById('login-btn').addEventListener('click', async () => {
        await login()
        if (token) loadTasks()
    })
})

async function loadTasks() {
    document.getElementById('pageloader').classList.add('is-active')
    toSection('section-tasks')

    const tasks_response = await fetch(`${baseUrl}/submissions/${userId()}?token=${token}`)
    if (!tasks_response.ok) {
        const failure = document.getElementById('failure')
        failure.innerHTML = strings[lang].invalid_token
        show('failure')
    } else {
        const tasks = await tasks_response.json()

        const content = document.getElementById('tasks-content')
        content.innerHTML = `<p>${strings[lang].logged_in_as} <code>${userId()}</code></p>`
        content.innerHTML += await genericDbInfo()
        content.innerHTML += `<h1 id=resultStats>${strings[lang].achieved_results}</h1>`

        let prev_category = "";
        for (let t of tasks) {
            let s = ""
            if (t['category_' + lang] != prev_category || !prev_category) {
                s += `<h1>${t['category_' + lang]}</h1>`
                prev_category = t['category_' + lang]
            }

            s += `<div id=task-${t.n} class=task>
                <button id="task-collapsible-${t.n}" class=collapsible>
                    <h2><span class=n>${t.n}</span>${t['title_' + lang]}<span class=correct-per-task></span></h2>
                    <span><i class="fas ${icons.down}"></i></span>
                </button>
                <div class=collapsible-content>
                    <div><ul>
                        <li><span class=cat>${strings[lang].output}:</span> ${t['output_' + lang]}
                        ${t['hint_' + lang] ? `<li><span class=cat>${strings[lang].hint}:</span> ${t['hint_' + lang]}` : ''}
                        <li><span class=cat>${strings[lang].sort}:</span> ${t['sort_' + lang]}</ul>`

            // tabs
            s += '<div class="tabs"><ul>'
            supported_ql.forEach((ql, i) => s += `<li data-task=${t.n} data-tab=${ql.id} ${i == 0 ? 'class="is-active"' : ''}><a>${ql.label}</a></li>`)
            s += '</ul></div>'
            s += '<div class=tab-content>'

            // text area and buttons
            supported_ql.forEach((ql, i) => {
                s += `<div data-content=${ql.id} class=${i == 0 ? 'is-active' : ''}>`
                const index = t.submissions.findIndex(s => s.ql == ql.id)
                const submission = t.submissions[index]
                const text = submission ? t.submissions[index].text : ''
                const correct = submission && submission.correct ? 'correct' : ''
                if (ql.id == "mongo") {
                    const collection = submission ? t.submissions[index].collection : ''
                    s += `<input type=text id=collection:${ql.id}:${t.n} value="${collection}" placeholder=collection class="input ${correct}">`
                }
                s += `<textarea id=submission:${ql.id}:${t.n} class="textarea ${correct}" maxlength=2000 placeholder="${ql.placeholder}">${text ?? ''}</textarea>
                  <div class=buttons>                    
                    <button class="button submit-btn is-success" data-ql=${ql.id} data-task=${t.n}>${strings[lang].save_execute}</button>
                    <button class="button reset-btn" data-ql=${ql.id} data-task=${t.n}>${strings[lang].reset}</button>
                  </div>`

                // result
                if (submission && submission.text) s += createResult(t, submission)

                // feedback
                s += createFeedback(t, submission, ql.id)

                s += '</div>' // data-content
            })
            s += '</div></div></div></div>' // tab-content, div, collapsible-content, task
            content.innerHTML += s
            countCorrect(t.n)
        }

        // final feedback
        content.innerHTML += await createFinalFeedback()
        document.querySelectorAll("#final-feedback select, #final-feedback textarea").forEach(i => {
            i.addEventListener("change", async (e) => {
                const post = {
                    task_id: "final-feedback",
                    which_lang_is_easy: document.getElementById('which_lang_is_easy').value,
                    which_lang_to_use: document.getElementById('which_lang_to_use').value,
                    which_other_lang: document.getElementById('which_other_lang').value,
                }
                try {
                    const response = await fetch(`${baseUrl}/final_feedback?token=${token}`, {
                        method: "POST",
                        body: JSON.stringify(post),
                        headers: {'Content-Type': 'application/json'}
                    })
                } catch (e) {
                    console.error(strings[lang].failed_execution + ': ' + e)
                }
            })
        })

        // auto-fit textareas on input
        const textareas = document.getElementsByTagName("textarea")
        for (let t of textareas) {
            t.style.height = (t.scrollHeight + 2) + "px"
            t.addEventListener("input", autoSizeTextarea)
        }

        // register collapsibles
        for (let t of tasks) {
            registerCollapsible('task', t.n)
            t.submissions.forEach(s => {
                if (s.text) registerCollapsible('result', t.n, s.ql)
            })
            supported_ql.forEach((ql) => registerCollapsible('feedback', t.n, ql.id))
        }
        resultStats()
        registerCollapsible('task', 'feedback')
        initTabs()
        inputResizing()
        tabChange()

        // submit input
        document.querySelectorAll(".submit-btn").forEach(i => {
            i.addEventListener("click", async (e) => {
                const ql = e.target.getAttribute('data-ql')
                const task_id = e.target.getAttribute('data-task')
                const input = document.getElementById(`submission:${ql}:${task_id}`)
                const collection = document.getElementById(`collection:${ql}:${task_id}`)
                const text = input.value
                let submission = null
                try {
                    const post = {ql: ql, task_id: 'task-' + task_id, text: text}
                    if (ql == "mongo") post['collection'] = collection.value
                    const response = await fetch(`${baseUrl}/submissions?token=${token}`, {
                        method: "POST",
                        body: JSON.stringify(post),
                        headers: {'Content-Type': 'application/json'}
                    })
                    submission = await response.json()
                } catch (e) {
                    console.error(strings[lang].failed_execution + ': ' + e)
                }

                const container = i.parentElement.parentElement
                const result = container.querySelector('.result')
                if (result) result.remove()

                if (submission) {
                    if (!submission.text) submission.error = strings[lang].no_input + '.'

                    const t = tasks.find(t => t.n == task_id)
                    const index = t.submissions.findIndex(s => s.ql == submission.ql)
                    t.submissions[index] = submission

                    container.querySelector('.feedback').insertAdjacentHTML('beforebegin', createResult(t, submission))
                    localStorage.setItem(`result-collapsible-${t.n}-${submission.ql}`, "true")
                    registerCollapsible('result', t.n, submission.ql)

                    if (submission.correct) {
                        input.classList.add("correct")
                        if (collection) collection.classList.add("correct")
                    } else {
                        input.classList.remove("correct")
                        if (collection) collection.classList.remove("correct")
                    }
                    countCorrect(task_id)

                    const lastSave = container.querySelector('.last-save')
                    // lastSave.style.backgroundColor = 'var(--primary)'
                    // window.setTimeout(() => lastSave.style.backgroundColor = 'transparent', 2000)
                    lastSave.style.fontWeight = 600
                    window.setTimeout(() => lastSave.style.fontWeight = 400, 2000)
                }
                resultStats()
            })
        })

        // reset input
        document.querySelectorAll(".reset-btn").forEach(i => {
            i.addEventListener("click", async (e) => {
                const ql = e.target.getAttribute('data-ql')
                const task_id = e.target.getAttribute('data-task')
                const input = document.getElementById(`submission:${ql}:${task_id}`)
                const task = tasks.find((t) => t.n == task_id)
                const submission = task.submissions.find((s) => s.ql == ql)
                input.value = submission && submission.text || ''
                if (ql == "mongo") {
                    const collection = document.getElementById(`collection:${ql}:${task_id}`)
                    collection.value = submission && submission.collection || ''
                }
                autoSizeTextarea(input)
            })
        })

        // register feedback submission
        document.querySelectorAll(".feedback.for-task select, .feedback.for-task input").forEach(i => {
            i.addEventListener("change", async (e) => {
                const f = e.target.closest('.feedback')
                const [type, collapsible, task_id, ql] = f.firstElementChild.id.split('-')
                const feedback = {
                    understandability: parseInt(f.querySelector('.understandability').value),
                    difficulty: parseInt(f.querySelector('.difficulty').value),
                    required_time: parseInt(f.querySelector('.required_time').value),
                    reason_why_not: parseInt(f.querySelector('.reason_why_not').value)
                }
                try {
                    const post = {ql: ql, task_id: 'task-' + task_id, feedback: feedback}
                    const response = await fetch(`${baseUrl}/feedback?token=${token}`, {
                        method: "POST",
                        body: JSON.stringify(post),
                        headers: {'Content-Type': 'application/json'}
                    })
                } catch (e) {
                    console.error(strings[lang].failed_execution + ': ' + e)
                }
            })
        })
    }
    document.getElementById('pageloader').classList.remove('is-active')
}

async function createFinalFeedback() {
    const response = await fetch(`${baseUrl}/final_feedback/${userId()}?token=${token}`)
    const f = await response?.json()
    return `<div id=final-feedback class="task feedback">
        <button id="task-collapsible-feedback" class=collapsible>
            <h2>${strings[lang].final_feedback}</h2>
            <span><i class="fas ${icons.down}"></i></span>
        </button>
        <div class=collapsible-content>
            <div><span>${strings[lang].which_lang_is_easy}</span>
                <select id=which_lang_is_easy class="select is-small">
                    <option disabled hidden selected value>
                    ${supported_ql.map(ql => `<option value=${ql.id} ${f?.which_lang_is_easy == ql.id ? 'selected' : ''}>${ql.label}`).join('')}
                </select>
            </div>
            <div><span>${strings[lang].which_lang_to_use}</span>
                <select id=which_lang_to_use class="select is-small">
                    <option disabled hidden selected value>
                    ${supported_ql.map(ql => `<option value=${ql.id} ${f?.which_lang_to_use == ql.id ? 'selected' : ''}>${ql.label}`).join('')}
                </select>
            </div>
            <div><span>${strings[lang].which_other_lang}</span>
                <textarea id=which_other_lang class=textarea maxlength=2000>${f?.which_other_lang ?? ''}</textarea>
            </div>
        </div></div>`
}

async function genericDbInfo() {
    return `<h1>${strings[lang].db_info}</h1>
        <ul>
            <li>${strings[lang].db_info_data_source}
            <li>${strings[lang].db_info_access}
                <ul>
                    <li><b>MariaDB</b> – <span class=has-tooltip-arrow data-tooltip="CLI: mysqlsh -h dbql.dev -u mondial -p -D mondial --sql">
                        Host: dbql.dev, Port: 3306, User: mondial, Password: swordfish</span>, 
                        <a href=https://dbql.dev/adminer/?server=localhost&username=mondial&db=mondial target=_blank>Web UI</a>
                    <li><b>MongoDB</b> – <span class=has-tooltip-arrow data-tooltip="CLI: mongosh mongodb://mondial@dbql.dev:27017/mondial?authSource=mondial">
                        Host: dbql.dev, Port: 27017, User: mondial, Password: swordfish, Authentication Source: mondial</span>,
                        <a href=https://dbql.dev/adminer/?mongo=localhost&username=mondial&db=mondial target=_blank>Web UI</a>
                    <li><b>Neo4j</b> – <span class=has-tooltip-arrow data-tooltip="CLI: cypher-shell -a dbql.dev -u mondial">
                        Host: dbql.dev, Port: 7687, User: mondial, Password: swordfish</span>,
                        <a href=https://dbql.dev/neo4j/ target=_blank>Web UI</a>
                </ul>
        </ul>`
}

async function resultStats() {
    const response = await fetch(`${baseUrl}/user_stats/${userId()}?token=${token}`)
    const text = await response?.text()

    const results = document.getElementById("resultStats")
    results.nextElementSibling.remove()
    results.insertAdjacentHTML('afterend', text ? `${userStats(JSON.parse(text))}` : `<p>${strings[lang].no_results}</p>`)
}