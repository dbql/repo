import '../common.js'

document.addEventListener('DOMContentLoaded', () => {
    queryDbsVersions()

    document.getElementById("not-authorized").innerHTML = strings[lang].invalid_token
    document.querySelector(".menu-label").innerHTML = strings[lang].submissions

    if (token) loadTasks()

    document.getElementById('login-btn').addEventListener('click', async () => {
        await login()
        if (token) loadTasks()
    })
})

async function loadTasks() {
    toSection('section-tasks')
    const users_response = await fetch(`${baseUrl}/users?token=${token}`)

    if (!users_response.ok) {
        show('not-authorized')
    } else {
        const users = await users_response.json()

        const content = document.getElementById('tasks-content')
        content.innerHTML = `<p>${strings[lang].logged_in_as} <code>${userId()}</code></p>`

        // user list
        let s = '', i = 1
        for (let user of users) {
            s += `<li><a href="?user=${user._id}">${i++}. ${user._id}</a></li>`
        }
        document.getElementById('user-list').innerHTML = s

        // selected user's exam submission
        const selectedUser = getQueryParam('user')
        if (!selectedUser) {
            document.getElementById('tasks-content').innerHTML = `<p>${strings[lang].select_submission}.</p>`
            return
        }

        const tasks_response = await fetch(`${baseUrl}/submissions/${selectedUser}?token=${token}`)
        const tasks = await tasks_response.json()

        content.innerHTML += `<h1>${selectedUser}</h1>`

        // overall results
        const user = users.find(u => u._id == selectedUser)
        content.innerHTML += userStats(user)

        // detail results
        for (let t of tasks) {
            let s = `<div id=task-${t.n} class=task>
                <button id="task-collapsible-${t.n}" class=collapsible>
                    <h2><span class=n>${t.n}</span>${t['title_' + lang]}<span class=correct-per-task></span></h2>
                    <span><i class="fas ${icons.down}"></i></span>
                </button>
                <div class=collapsible-content>
                    <div><ul>
                        <li><span class=cat>${strings[lang].output}:</span> ${t['output_' + lang]}
                        ${t.hint ? `<li><span class=cat>${strings[lang].hint}:</span> ${t['hint_' + lang]}` : ''}
                        <li><span class=cat>${strings[lang].sort}:</span> ${t['sort_' + lang]}</ul>`

            // tabs
            s += '<div class="tabs"><ul>'
            supported_ql.forEach((ql, i) => s += `<li data-task=${t.n} data-tab=${ql.id} ${i == 0 ? 'class="is-active"' : ''}><a>${ql.label}</a></li>`)
            s += '</ul></div>'
            s += '<div class=tab-content>'

            // text area and buttons
            supported_ql.forEach((ql, i) => {
                s += `<div data-content=${ql.id} class=${i == 0 ? 'is-active' : ''}>`
                const index = t.submissions.findIndex(s => s.ql == ql.id)
                const submission = t.submissions[index]
                const text = submission ? t.submissions[index].text : ''
                const correct = submission && submission.correct ? 'correct' : ''
                if (ql.id == "mongo") {
                    const collection = submission ? t.submissions[index].collection : ''
                    s += `<input disabled type=text id=collection:${ql.id}:${t.n} value="${collection}" placeholder=collection class="input ${correct}">`
                }
                s += `<textarea disabled id=submission:${ql.id}:${t.n} class="textarea ${correct}" maxlength=2000 placeholder="${ql.placeholder}">${text}</textarea>`

                // result
                if (submission && submission.text) s += createResult(t, submission)

                s += '</div>' // data-content
            })
            s += '</div></div></div></div>' // tab-content, div, collapsible-content, task
            content.innerHTML += s
            countCorrect(t.n)
        }

        // auto-fit textareas on input
        const textareas = document.getElementsByTagName("textarea")
        for (let t of textareas) t.style.height = (t.scrollHeight + 2) + "px"

        // register collapsibles
        for (let t of tasks) {
            registerCollapsible('task', t.n)
            t.submissions.forEach(s => {
                if (s.text) registerCollapsible('result', t.n, s.ql)
            })
        }
        initTabs()
        tabChange()
    }
}

function getQueryParam(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&')
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url)
    if (!results) return null
    if (!results[2]) return ''
    return decodeURIComponent(results[2].replace(/\+/g, ' '))
}