// global variables

const baseUrl = "https://dbql.dev/app"
const studentDomain = ""

const supported_ql = [
    {id: "sql", label: "MariaDB SQL", placeholder: "select ... from ..."},
    {id: "mongo", label: "MongoDB Aggregation Pipeline", placeholder: "[ pipeline ... ]"},
    {id: "cypher", label: "Neo4j Cypher", placeholder: "match (...) return ..."},
]
const icons = {"down": "fa-chevron-down", "up": "fa-chevron-up"}

let token = sessionStorage.getItem('token')
const userId = () => jwt_decode(token).mail

// global functions

const show = (id) => document.getElementById(id).style.display = 'block'
const hide = (id) => document.getElementById(id).style.display = 'none'
const showCollapsible = (element) => element.style.maxHeight = element.scrollHeight + "px"
const hideCollapsible = (element) => element.style.maxHeight = null

function toSection(id) {
    document.querySelectorAll(".section").forEach(i => {
        i.style.display = 'none'
    })
    document.getElementById(id).style.display = 'block'
}

function formatLastSave(ts) {
    return ts ? `${strings[lang].last_save}: ${new Date(ts).toLocaleString()}` : ''
}

function resultIcon(submission) {
    if (submission.correct) return '️✅'
    else if (submission.error) return '❌'
    return '⚠️'
}

function formatResultOutput(t, submission) {
    const own = submission.output ? JSON.parse(submission.output) : []
    const ref = JSON.parse(t[submission.ql + '_ref'])
    const warning = submission.warning ? `<p class=warning>${strings[lang].max_rows_warning} ${submission.warning}.</p>` : ''

    const isEqual = (own, ref) => Array.isArray(ref) ? JSON.stringify(own) === JSON.stringify(ref) : own === ref

    for (let i in own) for (let j in own[i]) {
        const equal = ref && ref[i] && isEqual(own[i][j], ref[i][j])
        own[i][j] = {value: own[i][j], equal: equal}
    }
    for (let i in ref) for (let j in ref[i]) {
        const equal = own && own[i] && own[i][j] && isEqual(own[i][j].value, ref[i][j])
        ref[i][j] = {value: ref[i][j], equal: equal}
    }

    return `<div class=output-tables>
        <div><p>${strings[lang].own_result}</p>${warning} ${own ? toTable(own) : "-"}</div>
        <div><p>${strings[lang].ref_result}</p>${ref ? toTable(ref) : "-"}</div>
    </div>`
}

function autoSizeTextarea(t) {
    if (t.type != "textarea") {
        t = this
        t.style.height = 0
        t.style.height = (t.scrollHeight + 2) + "px"
    } else window.setTimeout(() => {
        t.style.height = 0
        t.style.height = (t.scrollHeight + 2) + "px"
    }, 10)
}

function toTable(array) {
    return `<table>${array.map(r => {
        const row = r.map(c => {
            const value = Array.isArray(c.value) ? `[${c.value}]` : c.value
            return `<td ${c.equal ? "" : "class=ne"}>${value}</td>`
        }).join("")
        return `<tr>${row}</tr>`
    }).join("")}</table>`
}

function inputResizing() {
    const textareas = document.querySelectorAll('textarea')
    textareas.forEach((t) => t.addEventListener("mousedown", function () {
        const task_id = this.id.split(":")[2] ?? 'feedback'
        const p = document.getElementById(`task-collapsible-${task_id}`).nextElementSibling
        p.style.maxHeight = "none"
    }))
    textareas.forEach((t) => t.addEventListener("mouseup input", function () {
        const task_id = this.id.split(":")[2] ?? 'feedback'
        const p = document.getElementById(`task-collapsible-${task_id}`).nextElementSibling
        showCollapsible(p)
    }))
}

function tabChange() {
    document.querySelectorAll(".tabs li > a").forEach(i => {
        i.addEventListener("click", function () {
            const ql = this.parentElement.getAttribute("data-tab")
            const task_id = this.parentElement.getAttribute("data-task")

            // resize textarea
            const t = document.getElementById(`submission:${ql}:${task_id}`)
            autoSizeTextarea(t)

            // navigate from a tab with no result to a tab with a result
            const p = document.getElementById(`task-collapsible-${task_id}`).nextElementSibling
            p.style.maxHeight = "none"

            // set saved collapsible status when tab is changed
            const collapsibles = [`result-collapsible-${task_id}-${ql}`, `feedback-collapsible-${task_id}-${ql}`]
            collapsibles.forEach(c => {
                if (localStorage.getItem(c)) {
                    window.setTimeout(() => {
                        const collapsible = document.getElementById(c)
                        if (collapsible) {
                            showCollapsible(collapsible.nextElementSibling)
                            const p = document.getElementById(`task-collapsible-${task_id}`).nextElementSibling
                            if (!p.style.maxHeight) showCollapsible(p)
                        }
                    }, 10)
                }
            })
        })
    })
}

async function queryDbsVersions() {
    const response = await fetch(`${baseUrl}/dbs_versions`)
    if (response.ok) {
        const versions = await response.json()
        const s = `<span>MariaDB ${(versions.mariadb ?? '').split('-')[0]}</span>
            <span>MongoDB ${versions.mongodb ?? ''}</span>
            <span>Neo4j ${(versions.neo4j ?? '').replace('Neo4j/', '')}</span>`
        document.querySelector('#versions').innerHTML = s
    }
}


function createResult(t, submission) {
    const id = `result-collapsible-${t.n}-${submission.ql}`
    return `<div class=result>
        <button id=${id} class=collapsible>
            <span>${strings[lang].result}</span> 
            <span class=icon>${resultIcon(submission)}</span> 
            <span><i class="fas ${icons.down}"></i></span>
            <span class=last-save>${formatLastSave(submission.date_created)}</span>
        </button>
        <div class=collapsible-content>${(submission.error) ? `<div class=error>${submission.error}</div>` : formatResultOutput(t, submission)}</div>
      </div>`
}

function createFeedback(t, s, ql) {
    const id = `feedback-collapsible-${t.n}-${ql}`
    return `<div class="feedback for-task">
        <button id=${id} class=collapsible>
            <span><i class="fas ${icons.down}"></i></span>
            <div>${strings[lang].feedback}</div> 
        </button>
        <div class=collapsible-content>
            <div><span>${strings[lang].understandability}:</span>
                <select class="understandability select is-small">
                    <option disabled hidden selected value>
                    <option value=1 ${s?.feedback?.understandability == 1 ? 'selected' : ''}>${strings[lang].worded_clearly}
                    <option value=2 ${s?.feedback?.understandability == 2 ? 'selected' : ''}>${strings[lang].worded_somewhat_unclearly}
                    <option value=3 ${s?.feedback?.understandability == 3 ? 'selected' : ''}>${strings[lang].worded_unclearly}
                </select>
            </div>
            <div><span>${strings[lang].difficulty}:</span>
                <select class="difficulty select is-small">
                    <option disabled hidden selected value>
                    <option value=1 ${s?.feedback?.difficulty == 1 ? 'selected' : ''}>${strings[lang].very_easy}
                    <option value=2 ${s?.feedback?.difficulty == 2 ? 'selected' : ''}>${strings[lang].easy}
                    <option value=3 ${s?.feedback?.difficulty == 3 ? 'selected' : ''}>${strings[lang].moderate}
                    <option value=4 ${s?.feedback?.difficulty == 4 ? 'selected' : ''}>${strings[lang].difficult}
                    <option value=5 ${s?.feedback?.difficulty == 5 ? 'selected' : ''}>${strings[lang].very_difficult}
                </select>
            </div>
            <div><span>${strings[lang].required_time}:</span>
                <input type=number class="required_time input is-small" min=1 max=1440 maxlength=4 value="${s?.feedback?.required_time ?? ''}"
                    oninput="if (parseInt(this.value) > this.max) this.value = this.max; if (parseInt(this.value) < this.min) this.value = this.min">
            </div>
            <div><span>${strings[lang].reason_why_not}:</span>
                <select class="reason_why_not select is-small">
                    <option disabled hidden selected value>
                    <option value=1 ${s?.feedback?.reason_why_not == 1 ? 'selected' : ''}>${strings[lang].worded_unclearly}
                    <option value=2 ${s?.feedback?.reason_why_not == 2 ? 'selected' : ''}>${strings[lang].very_difficult}
                    <option value=3 ${s?.feedback?.reason_why_not == 3 ? 'selected' : ''}>${strings[lang].very_easy}
                    <option value=4 ${s?.feedback?.reason_why_not == 4 ? 'selected' : ''}>${strings[lang].not_required}
                    <option value=5 ${s?.feedback?.reason_why_not == 5 ? 'selected' : ''}>${strings[lang].no_more_time}
                    <option value=6 ${s?.feedback?.reason_why_not == 6 ? 'selected' : ''}>${strings[lang].other_reason}                    
                </select>
            </div>            
        </div>
      </div>`
}

function registerCollapsible(prefix, task_id, ql) {
    const id = `${prefix}-collapsible-${task_id}${ql ? '-' + ql : ''}`
    document.getElementById(id).addEventListener("click", function () {
        this.classList.toggle("active")
        const icon = document.querySelector(`#${id} i`)
        if (icon.classList.contains(icons.down)) {
            icon.classList.remove(icons.down)
            icon.classList.add(icons.up)
            localStorage.setItem(id, "true")
        } else {
            icon.classList.remove(icons.up)
            icon.classList.add(icons.down)
            localStorage.removeItem(id)
        }
        const e = this.nextElementSibling
        if (e.style.maxHeight) hideCollapsible(e)
        else showCollapsible(e)

        // fit parent collapsible
        const parent_id = `task-collapsible-${task_id}`
        if (ql && localStorage.getItem(parent_id)) {
            const p = document.getElementById(parent_id).nextElementSibling
            showCollapsible(p)
        }
    })
    if (localStorage.getItem(id)) document.getElementById(id).click()
}

async function login() {
    let user_id = document.getElementById('username').value
    user_id = user_id.indexOf('@') == -1 ? `${user_id}${studentDomain}` : user_id
    const authResponse = await fetch(`${baseUrl}/auth`, {
        method: "POST",
        body: JSON.stringify({
            username: user_id,
            password: document.getElementById('password').value
        }),
        headers: {'Content-Type': 'application/json'}
    })
    if (!authResponse.ok) {
        show('login-failed')
    } else {
        token = await authResponse.text()
        sessionStorage.setItem('token', token)
    }
}

function countCorrect(task_id) {
    const correct = document.querySelectorAll(`#task-${task_id} textarea.correct`).length
    const e = document.querySelector(`#task-${task_id} .correct-per-task`)
    e.innerHTML = `${correct}&nbsp;/&nbsp;3`
}

function userStats(user) {
    let s = ''
    supported_ql.forEach(ql => {
        s += `<li><b>${ql.label}</b>: ${strings[lang].edited} ${user[ql.id + '_saved']}, ${strings[lang].correct} ${user[ql.id + '_correct']}`
    })
    s += `<li><b>${strings[lang].total}</b>: ${strings[lang].edited} ${user.saved}, ${strings[lang].correct} ${user.correct}`
    return `<ul>${s}</ul>`
}