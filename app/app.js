import express from 'express'
import {config} from './config.js'
import cors from 'cors'
import {MongoClient} from 'mongodb'
import https from 'https'
import ActiveDirectory from 'activedirectory'
import jwt from 'jsonwebtoken'
import fs from 'fs'
import mariadb from 'mariadb'
import neo4j from 'neo4j-driver'
import {parseFilter} from 'mongodb-query-parser'
import * as path from 'path'
import { fileURLToPath } from 'url'

const app = express()
app.use(express.json())
const public_dir = path.dirname(fileURLToPath(import.meta.url)) + '/client'
app.use(express.static(public_dir))
if (config.cors) app.use(cors())

let conn_mongo_admin, col_tasks, col_submissions
let conn_mariadb, conn_mongo, conn_neo4j
const supported_ql = ["sql", "mongo", "cypher"]
const max_rows = 250

main().catch(e => console.error(e))

async function main() {

    // Application database
    const mongo_client_admin = await new MongoClient(config.db.connection).connect()
    conn_mongo_admin = mongo_client_admin.db(config.db.name)
    col_tasks = conn_mongo_admin.collection('tasks')
    col_submissions = conn_mongo_admin.collection('submissions')

    await col_submissions.createIndex({"user_id": 1})
    await col_submissions.createIndex({"task_id": 1})
    await col_submissions.createIndex({"ql": 1})

    // Mondial MariaDB
    conn_mariadb = await mariadb.createConnection(config.mondial.mariadb.connection)

    // Mondial MongoDB
    const mongo_client = new MongoClient(config.mondial.mongodb.connection)
    await mongo_client.connect()
    conn_mongo = mongo_client.db(config.mondial.mongodb.db)

    // Mondial Neo4J
    conn_neo4j = neo4j.driver(config.mondial.neo4j.connection, neo4j.auth.basic(config.mondial.neo4j.user, config.mondial.neo4j.password), {disableLosslessIntegers: true}).session({
        database: config.mondial.neo4j.db,
        defaultAccessMode: neo4j.session.READ
    })

    // Start HTTP server
    let sslOptions = {
        key: fs.readFileSync(config.ssl.key),
        cert: fs.readFileSync(config.ssl.cert)
    }
    https.createServer(sslOptions, app).listen(config.port)
    console.log(`Listening on port ${config.port}.`)
}

app.all('/create_tasks', async (req, res) => {
    if (!isAdmin(req.query.token, res)) return

    await col_tasks.drop().catch(() => 0)
    let tasks = JSON.parse(fs.readFileSync('./assets/tasks.json'))
    for (let t of tasks) {
        t._id = `task-${t.n}`
        if (t.sql_query) [t.sql_ref] = await sqlQuery(t.sql_query)
        if (t.mongo_query) [t.mongo_ref] = await mongoQuery(t.mongo_query.collection, t.mongo_query.pipeline)
        if (t.cypher_query) [t.cypher_ref] = await cypherQuery(t.cypher_query)
        await col_tasks.insertOne(t)
    }
    res.send(`Created ${tasks.length} tasks.`)
})

app.post('/auth', (req, res) => {
    if (config.demoUsersEnabled && inDemoUsers(req.body.username.toLowerCase(), req.body.password)) {
        const token = jwt.sign({mail: req.body.username.toLowerCase()}, config.jwtSecret)
        res.send(token)
    } else if (config.activeDirectoryEnabled) {
        const ad = new ActiveDirectory(config.activeDirectory)
        const mail = req.body.username.toLowerCase()
        ad.authenticate(mail, req.body.password, function (err, auth) {
            if (err || !auth) {
                res.sendStatus(401)
            } else {
                ad.findUser(mail, function (err, user) {
                    if (err) {
                        res.sendStatus(401)
                    } else {
                        const token = jwt.sign({
                            mail: mail,
                            lastName: user.sn,
                            firstName: user.givenName
                        }, config.jwtSecret)
                        res.send(token)
                    }
                })
            }
        })
    } else {
        res.sendStatus(401)
    }
})

app.get('/submissions/:user_id', async (req, res) => {
    const user = validateToken(req.query.token, res)
    const user_id = req.params.user_id
    if (user_id != user.mail && !isAdmin(req.query.token, res)) return

    const result = await col_tasks.aggregate([
        {
            $lookup: {
                from: "submissions", let: {task_id: "$_id"}, pipeline: [
                    {$match: {$expr: {$and: [{$eq: ["$task_id", "$$task_id"]}, {$eq: ["$user_id", user_id]}]}}}
                ], as: "submissions"
            }
        },
        {$sort: {"n": 1}},
        {$project: {_id: 0, sql_query: 0, mongo_query: 0, cypher_query: 0}}
    ]).toArray()
    res.send(result)
})

const user_stats_pipeline_stage = {
    $group: {
        _id: "$user_id",
        sql_saved: {$sum: {$cond: [{$eq: ["$ql", "sql"]}, 1, 0]}},
        sql_correct: {$sum: {$cond: [{$and: ["$correct", {$eq: ["$ql", "sql"]}]}, 1, 0]}},
        mongo_saved: {$sum: {$cond: [{$eq: ["$ql", "mongo"]}, 1, 0]}},
        mongo_correct: {$sum: {$cond: [{$and: ["$correct", {$eq: ["$ql", "mongo"]}]}, 1, 0]}},
        cypher_saved: {$sum: {$cond: [{$eq: ["$ql", "cypher"]}, 1, 0]}},
        cypher_correct: {$sum: {$cond: [{$and: ["$correct", {$eq: ["$ql", "cypher"]}]}, 1, 0]}},
        saved: {$count: {}},
        correct: {$sum: {$cond: ["$correct", 1, 0]}},
    }
};

app.get('/user_stats/:user_id', async (req, res) => {
    const user = validateToken(req.query.token, res)
    const user_id = req.params.user_id
    if (user_id != user.mail && !isAdmin(req.query.token, res)) return

    const result = await col_submissions.aggregate([
        {$match: {user_id: user_id}},
        user_stats_pipeline_stage
    ]).toArray()
    res.send(result[0])
})

app.get('/users/:order?/:desc?', async (req, res) => {
    if (!isAdmin(req.query.token, res)) return

    const result = await col_submissions.aggregate([
        user_stats_pipeline_stage,
        {$sort: {[req.params.order ?? '_id']: req.params.desc == 'desc' ? -1 : 1}}
    ]).toArray()
    res.send(result)
})

app.post('/submissions', async (req, res) => {
    const user = validateToken(req.query.token, res)

    const task = await col_tasks.findOne({_id: req.body.task_id})
    if (!task || !supported_ql.includes(req.body.ql)) {
        res.sendStatus(404)
        return
    }

    const ql = req.body.ql
    const filter = {user_id: user.mail, task_id: req.body.task_id, ql: ql}
    const doc = (await col_submissions.findOne(filter)) || {...filter}
    doc.date_created = Date.now()
    doc.text = req.body.text
    if (req.body.collection) doc.collection = req.body.collection

    delete doc.correct
    if (doc.text) {
        delete doc.warning
        let rows
        try {
            if (ql == "sql") [doc.output, rows] = await sqlQuery(doc.text)
            else if (ql == "mongo") [doc.output, rows] = await mongoQuery(doc.collection, doc.text)
            else if (ql == "cypher") [doc.output, rows] = await cypherQuery(doc.text)
            delete doc.error
            if (rows > max_rows) doc.warning = rows
        } catch (e) {
            doc.error = e.message || e.text
            if (doc.error) doc.error = doc.error.replace(mariadb_max_statement_time_prefix, '')
            delete doc.output
        }

        const ref_output = task[`${ql}_ref`]
        if (doc.output == ref_output) doc.correct = true
    } else {
        delete doc.output
    }

    await col_submissions.replaceOne(filter, doc, {upsert: true})
    res.send(doc)
})

app.post('/feedback', async (req, res) => {
    const user = validateToken(req.query.token, res)

    const task = await col_tasks.findOne({_id: req.body.task_id})
    if (!task || !supported_ql.includes(req.body.ql)) {
        res.sendStatus(404)
        return
    }

    const filter = {user_id: user.mail, task_id: req.body.task_id, ql: req.body.ql}
    const doc = (await col_submissions.findOne(filter)) || {...filter}

    doc.feedback = req.body.feedback
    doc.feedback.date_created = Date.now()
    Object.keys(doc.feedback).forEach(k => !doc.feedback[k] && delete doc.feedback[k])

    await col_submissions.replaceOne(filter, doc, {upsert: true})
    res.send(doc)
})

app.get('/final_feedback/:user_id', async (req, res) => {
    const user = validateToken(req.query.token, res)
    const result = await col_submissions.findOne({"user_id": user.mail, "task_id": "final-feedback"}) || {}
    res.send(result)
})

app.post('/final_feedback', async (req, res) => {
    const user = validateToken(req.query.token, res)
    const filter = {user_id: user.mail, task_id: 'final-feedback'}
    const doc = (await col_submissions.findOne(filter)) || {...filter}
    for (const [key, value] of Object.entries(req.body)) doc[key] = value
    await col_submissions.replaceOne(filter, doc, {upsert: true})
    res.send(doc)
})

app.get('/dbs_versions', async (req, res) => {
    const mariadb = await conn_mariadb.query('select version() as version')
    const mongodb = await conn_mongo_admin.admin().serverStatus()
    let neo4j
    try {
        neo4j = await conn_neo4j.run("return 1")
    } catch (e) {
    }
    const versions = {
        mariadb: mariadb[0].version,
        mongodb: mongodb.version,
        neo4j: neo4j ? neo4j.summary.server.agent : "not available"
    }
    res.send(versions)
})

function validateToken(token, res) {
    try {
        const decoded = jwt.verify(token, config.jwtSecret)
        return decoded
    } catch (e) {
        res.sendStatus(401)
        return false
    }
}

function isAdmin(token, res) {
    const user = validateToken(token, res)
    if (!user) return false
    if (!config.admins.includes(user.mail)) {
        res.sendStatus(403)
        return false
    }
    return true
}

const mariadb_max_statement_time_prefix = 'set statement max_statement_time=3 for '

async function sqlQuery(query, log = false) {
    if (log) console.log(query)
    let res = await conn_mariadb.query({nestTables: '_', sql: mariadb_max_statement_time_prefix + query})
    let rows = res.slice(0, max_rows).map((row) => Object.values(row))
    return [serialize(rows), res.length]
}

async function cypherQuery(query, log = false) {
    if (log) console.log(query)
    let res = await conn_neo4j.run(query)
    let rows = res.records.map((record) => record._fields).slice(0, max_rows)
    return [serialize(rows), res.records.length]
}

async function mongoQuery(collection, pipeline, log = false) {
    if (log) console.log(`collection = ${collection}, pipeline = ${pipeline}`)

    let response = await conn_mongo.command({
        "aggregate": collection,
        "pipeline": parseFilter(pipeline),
        "cursor": {"batchSize": 1e4},
        "maxTimeMS": 3000
    })
    const res = response.cursor.firstBatch
    const keys = res.map((row) => Object.keys(row)).flat(1).filter(deduplicate).sort()
    const rows = res.map((row) => keys.map((k) => row[k] ?? null))
    return [serialize(rows.slice(0, max_rows)), rows.length]
}

function inDemoUsers(username, password) {
    for (let d of config.demoUsers) {
        if (d.name == username && d.password == d.password) return true
    }
    return false
}

// const serialize = (array) => table(array).toString().replaceAll("null", "    ")
// const serialize = (array) => array.map((row) => row.join(",")).join("\n")
const serialize = (array) => JSON.stringify(array, replacer);
const replacer = (k, v) => {
    return typeof v === 'bigint' || typeof v === 'number' ? v.toString() : v;
}
const deduplicate = (value, index, self) => self.indexOf(value) === index